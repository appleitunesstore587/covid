import { AxiosError } from 'axios';
import React from 'react';

type ContentWrapperProps = {
  isLoading: boolean;
  error?: AxiosError;
};

export type ContentWrapperComponent = React.FC<ContentWrapperProps>;
