import { ContentWrapperComponent } from './ContentWrapper.types';
import { Box, CircularProgress } from '@mui/material';

export const ContentWrapper: ContentWrapperComponent = ({ isLoading, error, children }) => {
  if (isLoading) {
    return (
      <Box
        sx={{ display: 'flex' }}
        flex="1 1 auto"
        height="100vh"
        alignItems="center"
        justifyContent="center"
      >
        <CircularProgress size={150} thickness={3} />
      </Box>
    );
  }

  if (!isLoading && error) {
    return (
      <Box
        sx={{ display: 'flex' }}
        flex="1 1 auto"
        height="100vh"
        alignItems="center"
        justifyContent="center"
      >
        An error occurred! Please try later!
      </Box>
    );
  }

  return <>{children || null}</>;
};
