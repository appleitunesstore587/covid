import styled from 'styled-components';

export const AppWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1 1 auto;
  width: 100vw;

  @media (max-width: 900px) {
    flex-direction: column;
    overflow: auto;

    > div:first-child {
      margin-bottom: 16px;
      width: 100vw;
      flex: none;

      > div > div {
        position: relative;
        width: 100vw;

        > ul {
          padding: 0;
        }
      }
    }

    > div:nth-child(2) {
      height: auto;
      width: 100vw;
    }
  }
`;
