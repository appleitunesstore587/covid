import React from 'react';
import { WorldWIPFilters } from '../../services/api/types';
import { usePeriodStart } from '../../hooks/usePeriodStart/usePeriodStart';
import { usePeriodEnd } from '../../pages/WorldWIP/hooks/usePeriodEnd/usePeriodEnd';
import { useCasesFilter } from '../../hooks/useCasesFilter/useCasesFilter';
import { WorldWIPCachedFilters } from './WorldWIPContext.types';

export const WorldWIPContext = React.createContext<WorldWIPCachedFilters>({
  start: new Date(),
  end: new Date(),
  filter: WorldWIPFilters.totalConfirmed,
  handleStartChange: () => void 0,
  handleEndChange: () => void 0,
  handleFilterChange: () => void 0,
});

export const WorldWIPProvider: React.FC = ({ children }) => {
  const { start, handleStartChange } = usePeriodStart();
  const { end, handleEndChange } = usePeriodEnd();
  const { filter, handleFilterChange } = useCasesFilter<WorldWIPFilters>(
    WorldWIPFilters.totalConfirmed,
  );

  return (
    <WorldWIPContext.Provider
      value={{
        start,
        end,
        filter,
        handleStartChange,
        handleEndChange,
        handleFilterChange,
      }}
    >
      {children}
    </WorldWIPContext.Provider>
  );
};
