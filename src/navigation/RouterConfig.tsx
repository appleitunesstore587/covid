import { Route, Switch } from 'react-router';
import { Suspense, lazy } from 'react';
import { CircularProgress } from '@mui/material';

import { Sidebar } from '../components/Sidebar/Sidebar';
import { ABOUT, COUNTRY_STAT, WORLD_WIP } from './CONSTANTS';

const About = lazy(() => import('../pages/About/About'));
const LiveByCountry = lazy(() => import('../pages/LiveByCountry/LiveByCountry'));
const WorldWIP = lazy(() => import('../pages/WorldWIP/WorldWIP'));

export default function RouterConfig() {
  return (
    <Suspense fallback={<CircularProgress size={150} thickness={3} />}>
      <Sidebar />
      <Switch>
        <Route exact path={WORLD_WIP} component={WorldWIP} />
        <Route exact path={COUNTRY_STAT} component={LiveByCountry} />
        <Route exact path={ABOUT} component={About} />
      </Switch>
    </Suspense>
  );
}
