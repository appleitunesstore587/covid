import React from 'react';
import { WorldWIPDto, WorldWIPFilters } from '../../../services/api/types';
import { SelectChangeEvent } from '@mui/material/Select';

type WorldWIPUIProps = {
  data: WorldWIPDto[];
  start: Date;
  end: Date;
  filter: WorldWIPFilters;
  handleStartChange: (date: Date | null) => void;
  handleEndChange: (date: Date | null) => void;
  handleFilterChange: (event: SelectChangeEvent) => void;
};

export type WorldWIPUIComponent = React.FC<WorldWIPUIProps>;
