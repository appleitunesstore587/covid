import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import { WorldWIPUIComponent } from './WorldWIPUI.types';
import { Box, MenuItem, Select, TextField, Typography } from '@mui/material';
import DateAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import { WorldWIPFilters } from '../../../services/api/types';
import { SelectsWrapper } from '../../../components/SelectsWrapper/SelectsWrapper';

export const WorldWIPUI: WorldWIPUIComponent = ({
  data,
  start,
  end,
  filter,
  handleStartChange,
  handleEndChange,
  handleFilterChange,
}) => {
  return (
    <Box
      sx={{ display: 'flex' }}
      height="100vh"
      width="calc(100vw - 300px)"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      px={4}
    >
      <Typography variant="h4">World WIP</Typography>
      <Box mb={2} />
      <ResponsiveContainer width="99%" height={500}>
        <LineChart
          width={730}
          height={400}
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <CartesianGrid strokeDasharray="4" />
          <XAxis dataKey="Date" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey={filter} stroke="#8884d8" />
        </LineChart>
      </ResponsiveContainer>
      <SelectsWrapper>
        <LocalizationProvider dateAdapter={DateAdapter}>
          <DesktopDatePicker
            label="Period start"
            inputFormat="MM/dd/yyyy"
            value={start}
            onChange={handleStartChange}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <LocalizationProvider dateAdapter={DateAdapter}>
          <DesktopDatePicker
            label="Period end"
            inputFormat="MM/dd/yyyy"
            value={end}
            onChange={handleEndChange}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <Select id="case-select" value={filter} label="Case" onChange={handleFilterChange}>
          {Object.values(WorldWIPFilters).map((filter) => (
            <MenuItem value={filter} key={filter}>
              {filter.includes('New')
                ? `New ${filter.slice(3).toLowerCase()}`
                : `Total ${filter.slice(5).toLowerCase()}`}
            </MenuItem>
          ))}
        </Select>
      </SelectsWrapper>
    </Box>
  );
};
