import { useState } from 'react';

export const usePeriodEnd = () => {
  const [end, setEnd] = useState<Date | null>(new Date());

  const handleEndChange = (date: Date | null) => {
    setEnd(date);
  };

  return {
    end,
    handleEndChange,
  };
};
