import { ContentWrapper } from '../../components/ContentWrapper/ContentWrapper';
import { AxiosError } from 'axios';
import { useGetLiveByCountry } from './hooks/useGetLiveByCountry/useGetLiveByCountry';
import { useTitle } from '../../hooks/useTitle/useTitle';
import { CountryDto, LiveByCountryStatus } from '../../services/api/types';
import { LiveByCountryUI } from './LiveByCountryUI/LiveByCountryUI';
import { transformDate } from '../../utils/transformDate';
import { groupByDate } from './utils/groupByDate';
import { useContext } from 'react';
import { LiveByCountryContext } from '../../contexts/LiveByCountryContext/LiveByCountryContext';

export default function LiveByCountry() {
  useTitle('Live By Country And Status After Date');

  const {
    start,
    handleStartChange,
    country,
    handleCountryChange,
    filter,
    handleFilterChange,
    countries,
    isFetchingCountries,
    countriesError,
  } = useContext(LiveByCountryContext);

  const {
    data: liveByCountry,
    isLoading: isFetchingLiveByCountry,
    error: liveByCountryError,
  } = useGetLiveByCountry(country, start?.toISOString() as string, LiveByCountryStatus[filter]);

  return (
    <ContentWrapper
      isLoading={isFetchingCountries || isFetchingLiveByCountry}
      error={(countriesError || liveByCountryError) as AxiosError}
    >
      <LiveByCountryUI
        data={groupByDate(transformDate(liveByCountry))}
        start={start as Date}
        country={country}
        countries={countries as CountryDto[]}
        filter={filter}
        handleStartChange={handleStartChange}
        handleCountryChange={handleCountryChange}
        handleFilterChange={handleFilterChange}
      />
    </ContentWrapper>
  );
}
