import React from 'react';
import { CountryDto, GroupedLives, LiveByCountryFilters } from '../../../services/api/types';
import { SelectChangeEvent } from '@mui/material/Select';

type LiveByCountryUIProps = {
  data: GroupedLives[];
  start: Date;
  country: string;
  countries: CountryDto[];
  filter: LiveByCountryFilters;
  handleStartChange: (date: Date | null) => void;
  handleCountryChange: (event: SelectChangeEvent) => void;
  handleFilterChange: (event: SelectChangeEvent) => void;
};

export type LiveByCountryUIComponent = React.FC<LiveByCountryUIProps>;
