import { useQuery } from 'react-query';
import { getLiveByCountry } from '../../../../services/api/requests';
import { UseGetLiveByCountry } from './useGetLiveByCountry.types';

const QUERY_KEY = 'liveByCountry';

export const useGetLiveByCountry: UseGetLiveByCountry = (country, date_from, status) =>
  useQuery(
    [QUERY_KEY, country, date_from, status],
    async () => getLiveByCountry(country, date_from, status),
    {
      retry: false,
      staleTime: 3600000,
    },
  );
