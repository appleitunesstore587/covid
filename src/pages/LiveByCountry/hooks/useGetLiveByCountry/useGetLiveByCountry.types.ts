import { UseQueryResult } from 'react-query';
import { LiveByCountryDto } from '../../../../services/api/types';

export type UseGetLiveByCountry = (
  country: string,
  date_from: string,
  status: string,
) => UseQueryResult<LiveByCountryDto[]>;
