import { UseQueryResult } from 'react-query';
import { CountryDto } from '../../../../services/api/types';

export type UseGetCountries = () => UseQueryResult<CountryDto[]>;
