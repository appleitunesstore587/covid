import { useQuery } from 'react-query';
import { getCountries } from '../../../../services/api/requests';
import { UseGetCountries } from './useGetCountries.types';

const QUERY_KEY = 'countries';

export const useGetCountries: UseGetCountries = () =>
  useQuery([QUERY_KEY], async () => getCountries(), {
    retry: false,
  });
