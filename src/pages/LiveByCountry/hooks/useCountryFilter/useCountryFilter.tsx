import { useState } from 'react';
import { CountryDto } from '../../../../services/api/types';
import { SelectChangeEvent } from '@mui/material/Select';

export const useCountryFilter = () => {
  const [country, setCountry] = useState<CountryDto['Slug']>('ukraine');

  const handleCountryChange = (event: SelectChangeEvent) => {
    setCountry(event.target.value as CountryDto['Slug']);
  };

  return {
    country,
    handleCountryChange,
  };
};
