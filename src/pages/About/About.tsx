import { Link } from 'react-router-dom';
import { Box, Typography } from '@mui/material';
import { COUNTRY_STAT, WORLD_WIP } from '../../navigation/CONSTANTS';
import { AboutWrapper } from './About.styles';

export default function About() {
  return (
    <AboutWrapper flex="1 1 auto" p={4}>
      <Typography variant="h4">About covid app</Typography>
      <Box mb={2} />
      <Typography variant="body2">
        The app is a graphical representation of data on the Coronavirus, retrieved from covid19
        API.
      </Typography>
      <Typography variant="body2">
        Enjoy the <Link to={WORLD_WIP}>global statistics (World WIP)</Link> and{' '}
        <Link to={COUNTRY_STAT}>
          statistics by country (Live By Country And Status After Date).
        </Link>
      </Typography>
    </AboutWrapper>
  );
}
