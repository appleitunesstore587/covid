import { useTitle } from './useTitle';
import React from 'react';
import { render } from '@testing-library/react';

const expectedTitle = 'mockTitle';

const TestComponent: React.FC = () => {
  useTitle(expectedTitle);
  return <div>test</div>;
};

describe('useTitle', () => {
  document.title = 'initialTitle';
  it('changes page title', () => {
    expect(document.title).toEqual('initialTitle');
    render(<TestComponent />);
    expect(document.title).toEqual(`Covid19 | ${expectedTitle}`);
  });
});
