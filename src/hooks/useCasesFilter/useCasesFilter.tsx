import { useState } from 'react';
import { SelectChangeEvent } from '@mui/material/Select';

export const useCasesFilter = <Enum,>(initialFilter: Enum) => {
  const [filter, setFilter] = useState<Enum>(initialFilter);

  const handleFilterChange = (event: SelectChangeEvent) => {
    setFilter(event.target.value as unknown as Enum);
  };

  return {
    filter,
    handleFilterChange,
  };
};
