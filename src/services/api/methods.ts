import axios, { AxiosPromise } from 'axios';

export function methods() {
	const get = (url: string, options?: any): AxiosPromise => axios
		.get(url, options);
	
	return {
		get,
	};
}